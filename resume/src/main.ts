import App from './App.svelte';

//const data = require('./data.json');
import data from './history.json';


const app = new App({
	target: document.body,
	props: {
		data: data
	}
});

export default app;