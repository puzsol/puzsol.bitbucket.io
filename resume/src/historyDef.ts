export namespace History {

	export enum Mode {
		Personal,
		Employer,
		About,
		Tech,
		Print
	}

	export interface IMonthy {
		year: number;
		month: number;
	}

	export interface IEmployer {
		name: string;
		url: string;
		icon: string;
		about: string;
	}

	export interface ITechInfo {
		primary: Array<string>;
		tertiary?: Array<string>;
	}

	export interface IProject {
		title: string;
		about: string;
		impact: string;
	}

	export interface IContext {
		to: IMonthy;
		from: IMonthy;
		employer: IEmployer;
		roles: Array<string>;
		tech: ITechInfo;
		lang: ITechInfo;
		overview: string;
		projects: Array<IProject>;
		context: Array<IContext>;
		bubble: IBubble;
		parent: IContext | null;
	}

	export interface IBubble 
	{
		left: number;
		top: number;
		width: number;
		height: number;
		right: number;
		//ctx: History.IContext;
	}

	export interface IPicture {
		image: string;
		alt: string;
	}

	export interface IHistory {
		name: string;
		email: string;
		phone: string;
		picture: IPicture;
		about: Array<string>;
		aspiration: Array<string>;
		context: Array<IContext>;
	}

	export function map<T>(ctx: Array<IContext>, act: (c: IContext) => T): Array<T> {
		let ret:T[] = [];
		each(ctx, (c: IContext) => ret = [...ret, act(c)])
		return ret;
	}

	export const months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
	export const dtoj = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"].reverse();

	export function each(ctx: Array<IContext>, act: (c: IContext, p:IContext) => void, p:IContext = null) {
		if (ctx && ctx.length > 0){
			for(let c of ctx) {
				act(c, p);
				each(c.context, act, c);
			}
		}
	}

	export function parents(ctx: IContext): Array<IContext> {
		let ret:IContext[] = [];
		let p = ctx.parent; 
		while (p)
		{
			ret = [...ret, p];
			p = p.parent;
		}
		return ret;
	}

	/*export function sortBy<T>(fn: (c:IContext) => T): (l:IContext, r:IContext) => number {
		return 
	}*/

	export function sortByWidthDesc(l:IContext, r:IContext):number {
		return r.bubble.width - l.bubble.width;
	}

	export function sortByToDesc(l:IContext, r:IContext): number {
		let d = r.to.year - l.to.year;
		if (d == 0)
		{
			d = r.to.month - l.to.month;
		}
		if (d == 0)
		{
			d = cntMonth(r.from, r.to.year) - cntMonth(l.from, l.to.year);
		}
		return d;
	}

	export function hasOverlap(l:IContext): (r:IContext) => boolean {
		return (r:IContext) => l.bubble.left < r.bubble.right && r.bubble.left < l.bubble.right;
	}

	export function sortNoCase(l:string, r:string):number { return l.toLowerCase().localeCompare(r.toLowerCase()); }

	let year:number = 0;
	// Calculate the positions of the bubbles in the window
	export function cntMonth(m:History.IMonthy, year:number):number 
	{ 
		return  12 * (year - m.year) + 12 - m.month;
	}

	export function pxMonth(m:number):number 
	{
	 	return 16 * m + 8; 
	}

	function calcBubble(c:History.IContext): void 
	{ 
		const o = c.parent ? pxMonth(cntMonth(c.parent.to, year)) + 1 : 0;
		const l = pxMonth(cntMonth(c.to, year)) - o;
		const r = pxMonth(cntMonth(c.from, year)) - o;

		c.bubble = { 
			left: l, 
			top: 5,
			width: r-l-12, 
			height: 20,
			right: r//,
//			ctx: c 
		};
	}

	export function calcBubbles(ctx: Array<History.IContext>, y:number) {
		year = y;
		each(ctx, (c: History.IContext) => { calcBubble(c);	});
	}

	export function adjustSub(ctx: Array<History.IContext>, t:number): number | null
	{
		let m:number | null = null;
		if (ctx && ctx.length > 0)
		{
			var ord = [...ctx].sort(History.sortByToDesc);
			for(let i = 0; i < ord.length; ++i) {
				const c = ord[i];

				// Adjust our height to wrap around children
				c.bubble.height = (adjustSub(c.context, 25) ?? 14) + 1;

				// Find a gap that will fit our height without overlap
				const overlap = ord.slice(0, i).filter(History.hasOverlap(c));
				const tops = [t, ...overlap.map(c => c.bubble.top + c.bubble.height + 20)];
				const bots = [...overlap.map(c => c.bubble.top), 99999999];
				var top = t;
				for (let j=0; j < tops.length; ++j)
				{
					if (bots[j] - tops[j] > c.bubble.height) {
						top = tops[j];
						break;
					}
				}
				//var top = Math.max(t, ...ord.slice(0, i).filter(History.hasOverlap(c)).map((c) => c.bubble.top + c.bubble.height + 20));
				c.bubble.top = top;

				// Ensure we retain the max height
				m = Math.max(m, top + c.bubble.height);
			}
		}

		return m;
	}

	export function groom(h:IHistory):void {
		History.each(h.context, groomContext);
	}

	function groomTech(c:ITechInfo): void {
		if (c) {
			if (c.primary) {
				c.primary.sort(sortNoCase)
			} else {
				c.primary = [];
			}

			if (c.tertiary) {
				c.tertiary.sort(sortNoCase)
			} else {
				c.tertiary = [];
			}

			const dup = c.primary.filter(v => c.tertiary.includes(v));
			if (dup.length > 0)
			{
				throw new Error("Duplicated tech: " + dup);
			}
		}
	}

	export function nowMonthy(): IMonthy {
		const d = new Date(); 
		return { year: d.getFullYear(), month: d.getMonth() + 1 };
	}

	function groomContext(c:IContext, p:IContext): void {
		c.parent = p;
		if (!c.to) {
			c.to = nowMonthy();
		}

		if (c.projects) {
			c.projects.sort((l,r) => sortNoCase(l.title, r.title));
		}

		groomTech(c.lang);
		groomTech(c.tech);
	}

	
    export enum Tech {
        None = 0,
        Primary = 1,
		Secondary = 2,
		Both = 3
    }

    export interface ISegment {
        from: number;
        to: number;
        tech: Tech;
	}

	export function isVisible(seg:ISegment, t:Tech) {
		switch (t){
			case Tech.Primary:
				return seg.tech === Tech.Primary;
			case Tech.Secondary:
				return seg.tech === Tech.Secondary;			
			case Tech.Both:
				return true;
		}

		return false;
	}
	
	export function showTech(showPrimary: boolean, showTertiary: boolean) {
		if (showPrimary) {
			return showTertiary ? Tech.Both : Tech.Primary;
		}
		return showTertiary ? Tech.Secondary : Tech.None;
	}
    
    export class TechInfo {
        public byMonth: Array<Tech>;
        public allMonth: number = -1;
        public primaryMonth: number = -1;
        public tertiaryMonth: number = -1;

        constructor(public name: string) {
            this.byMonth = new Array<Tech>();
        }

        public year(t:Tech):number {
			switch (t){
				case Tech.Primary:
					return Math.floor(this.primaryMonth / 12);
				case Tech.Secondary:
					return Math.floor(this.tertiaryMonth / 12);			
			}

            return Math.floor(this.allMonth / 12);
        }

        public month(t:Tech):number {
			switch (t){
				case Tech.Primary:
					return this.primaryMonth % 12;
				case Tech.Secondary:
					return this.tertiaryMonth % 12;			
			}

			return this.allMonth % 12;        
		}

        public duration(t:Tech):string {
            const y = this.year(t);
            const m = this.month(t);
            
            return y > 0
                ? y + "y" + (m > 0 ? " - " + m + "m" : "")
                : (m > 0 ? m + "m" : "");
		}
		
		public isVisible(t:Tech, m:number): boolean {
			switch (t){
				case Tech.Primary:
					return this.primaryMonth >= m;
				case Tech.Secondary:
					return this.tertiaryMonth >= m;			
				case Tech.Both:
					return this.allMonth >= m;
			}

			return false;
		}

        public get segments():Array<ISegment> {
            let ret:Array<ISegment> = [];
            let from:number = null;
            let to:number = null;
            let tech:Tech = Tech.None;

            for (let i = this.byMonth.length - 1; i >= 0; --i) {
                if (this.byMonth[i] != tech) {
                    if (from && to && tech) {
                        ret = [...ret, {from, to, tech}];
                    }

                    tech = this.byMonth[i];
                    to = i;
                }

                from = i;
            }

            return ret;
        }

        public Add(origin:IMonthy, f:IMonthy, t:IMonthy, tech:Tech) {
			const dm = History.cntMonth(origin, origin.year) - 1;

            for (let i = cntMonth(t, origin.year); i <= cntMonth(f, origin.year); ++i)
            {
                if (this.byMonth[i-dm] !== Tech.Primary) {
                    this.byMonth[i-dm] = tech;
                }
			}
			
			this.allMonth = -1;
			this.primaryMonth = -1;
			this.tertiaryMonth = -1;
			for (let i of this.byMonth) {
				switch (i) {
					case Tech.Primary:
						++this.primaryMonth;
						++this.allMonth;
						break;
					case Tech.Secondary:
						++this.tertiaryMonth;
						++this.allMonth;
						break;
				}					
			}
        }
    }

    export class TechSet {
        constructor() {
            this.info = {};
        }

        private info:object;

        public get allTech():Array<TechInfo> {
            let arr:Array<TechInfo> = [];
            for (let i in this.info) {
                arr = [...arr, this.info[i]];
            }
            return arr.sort((l,r) => History.sortNoCase(l.name, r.name));
        }

        public getTech(name:string):TechInfo {
            var tech = this.info[name];
            if (!tech) {
                tech = new TechInfo(name);
                this.info[name] = tech;
            }
            return tech;
        }

        public addTech(origin:IMonthy, c:History.IContext, names:Array<string>, t:Tech): void {
            if (names) {
                for(var n of names) {
                    const i = this.getTech(n);
                    i.Add(origin, c.from, c.to, t);
                }
            }
        }
    }
}

