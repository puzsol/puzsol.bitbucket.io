# Resume for Andrew Watson

This is the [Svelte](https://svelte.dev) source for my resume, hosted on [Amazon S3](http://puzsol.resume.s3-website.us-east-2.amazonaws.com/).  It is a resume presentation app targetting the IT industry, but could be adapted for other employment types if desired.

It runs from a history.json file - which contains the information for each employer, sub-employer, project language, and technology used at each employer, for the duration employed.

At the moment, there is no management app, this is just the presentation layer.

It is covered by the MIT license