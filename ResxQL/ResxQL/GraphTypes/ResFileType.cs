﻿namespace ResxQL.GraphTypes
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;

    using HotChocolate.Resolvers;
    using HotChocolate.Types;

    using Resx;

    using Serilog;

    /// <summary>
    /// Resource File Type, gives a type name to the resource file, and configures an entry for every resource key it contains.
    /// This means defining a field or function for the name and the implementation to resolve it.
    /// </summary>
    public class ResFileType : ObjectType
    {
        private string _prefix;

        public ResFileType(string prefix, ResourceFileNameGroup rg)
        {
            _prefix = prefix;
            Group = rg;
        }

        public ResourceFileNameGroup Group { get; }

        protected override void Configure(IObjectTypeDescriptor descriptor)
        {
            descriptor.Name(_prefix + Group.Name.GraphQLSafe() + "_Messages");
            foreach (var k in Group.Keys)
            {
                Add(descriptor, k);
            }
        }

        private void Add(IObjectTypeDescriptor descriptor, ResKey k)
        {
            var def = k.For(CultureInfo.CurrentCulture.Name, CultureInfo.CurrentCulture.TwoLetterISOLanguageName);
            if (def == null)
            {
                // Only add strings that have english versions!
                return;
            }

            var field = descriptor.Field(k.Name).Description(def.Description ?? def.Text);
            var fields = new SortedSet<string>();
            if (def.HasTokens)
            {
                foreach (var t in def.Segments)
                {
                    if (t.Variable != null && fields.Add(t.Variable))
                    {
                        field.Argument(t.Variable, a => a.AddTypeFor(t.FormatType));
                    }
                }
            }

            field.Resolver(Get(def, k));
        }

        private Func<IResolverContext, string> Get(TokenString def, ResKey k)
        {
            if (def == null)
            {
                Log.Error("Resolver has no default value for key: {@key}", $"{Group.Name}.{k.Name}");
                return c => "Failure";
            }

            if (def.HasTokens)
            {
                return c => k.For(c.GetLanguages()).ConstructFunc()?.Invoke(c);
            }

            // Return a constant
            return c => k.For(c.GetLanguages()).Text;
        }
    }
}
