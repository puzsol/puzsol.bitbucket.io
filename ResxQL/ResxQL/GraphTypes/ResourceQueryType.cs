﻿namespace ResxQL.GraphTypes
{
    using System.IO;
    using System.Linq;
    using System.Reflection;

    using HotChocolate.Types;

    using Resx;

    using Serilog;

    /// <summary>
    /// Root query type for all the resource files.
    /// This will search the content folder for any resource files and serve them as a resource file type for each language group of files.
    /// </summary>
    public class ResourceQueryType : ObjectType
    {
        protected override void Configure(IObjectTypeDescriptor descriptor)
        {
            var contentPath = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) ?? string.Empty, "Content");

            Log.Information("Reading Resx files from {path}", contentPath);
            foreach (var ng in Directory
                .GetFiles(contentPath, "*.resx")
                .Select(x => new ResourceFileInfo(x))
                .GroupBy(x => x.Name, (k, i) => new ResourceFileNameGroup(k, i))
                .Where(x => x.Keys.Any()))
            {
                var rft = new ResFileType(string.Empty, ng);
                var fn = descriptor.Field(ng.Name.GraphQLSafe()).Type(rft);
                fn.Argument("language", a => a.Type<StringType>());
                fn.Resolver(c => 
                    { 
                        c.SetLanguage(c.Argument<string>("language"));
                        return rft;
                    });
            }

            foreach (var dir in Directory
                .GetDirectories(contentPath))
            {
                Log.Information("Reading Resx files from {path}", dir);

                var di = new DirectoryInfo(dir);
                var rff = new ResourceFolderType(string.Empty, di);
                var fn = descriptor.Field(di.Name.GraphQLSafe()).Type(rff);
                fn.Argument("language", a => a.Type<StringType>());
                fn.Resolver(c => 
                { 
                    c.SetLanguage(c.Argument<string>("language"));
                    return rff;
                });
            }
        }
    }
}
