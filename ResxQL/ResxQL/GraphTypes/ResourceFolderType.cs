﻿namespace ResxQL.GraphTypes
{
    using System.IO;
    using System.Linq;

    using HotChocolate.Types;

    using Resx;

    using Serilog;

    public class ResourceFolderType : ObjectType
    {
        private readonly DirectoryInfo _di;

        private readonly string _prefix;

        public ResourceFolderType(string prefix, DirectoryInfo di)
        {
            _di = di;
            _prefix = prefix;
        }

        protected override void Configure(IObjectTypeDescriptor descriptor)
        {
            descriptor.Name(_di.Name.GraphQLSafe() + "_Folder");

            foreach (var ng in _di
                .GetFiles("*.resx")
                .Select(x => new ResourceFileInfo(x.FullName))
                .GroupBy(x => x.Name, (k, i) => new ResourceFileNameGroup(k, i))
                .Where(x => x.Keys.Any()))
            {
                var rft = new ResFileType(_prefix + _di.Name.GraphQLSafe() + "_", ng);
                var fn = descriptor.Field(ng.Name.GraphQLSafe()).Type(rft);
                fn.Resolver(rft);
            }

            foreach (var di in _di.GetDirectories())
            {
                Log.Information("Reading Resx files from {path}", di.FullName);

                var rff = new ResourceFolderType(_prefix + _di.Name.GraphQLSafe() + "_", di);
                descriptor.Field(di.Name.GraphQLSafe()).Type(rff).Resolver(rff);
            }
        }
    }
}
