﻿namespace ResxQL.GraphTypes
{
    using System;
    using System.Globalization;
    using System.Linq;
    using System.Text.RegularExpressions;

    using HotChocolate.Resolvers;
    using HotChocolate.Types;

    using Resx;

    /// <summary>
    /// This class basically has the knowledge to work with our regex tokens and the Hot Chocolate IResolverContext
    /// </summary>
    public static class ResolverContextExtensions
    {
        private const string CultureKey = "culture";
        private static readonly Regex SafeName = new Regex(@"[\.\s]", RegexOptions.IgnoreCase);

        public static string[] GetLanguages(this IResolverContext c)
        {
            var culture = c.GetCulture() ?? CultureInfo.CurrentCulture;
            return (new[] { culture.Name, culture.TwoLetterISOLanguageName, CultureInfo.CurrentCulture.Name, CultureInfo.CurrentCulture.TwoLetterISOLanguageName }).Distinct().ToArray();
        }

        /// <summary>
        /// Function to get the language defined for this context
        /// </summary>
        public static CultureInfo GetCulture(this IResolverContext c)
        {
            if (c != null && c.ScopedContextData.TryGetValue(CultureKey, out var culture))
            {
                return culture as CultureInfo;
            }

            return null;
        }

        // Function to set the language defined for this context
        public static void SetLanguage(this IResolverContext c, string lang)
        {
            if (c != null && !string.IsNullOrWhiteSpace(lang))
            {
                try
                {
                    var culture = new CultureInfo(lang);
                    c.ScopedContextData = c.ScopedContextData
                        .SetItem(CultureKey, culture);
                }
                catch
                {
                    // nada
                }
            }
        }

        // Helper method to add the correct type for the token format as an argument
        public static void AddTypeFor(this IArgumentDescriptor a, TokenFormat tf)
        {
            switch (tf)
            {
                case TokenFormat.Int:
                    a.Type<NonNullType<IntType>>();
                    break;

                case TokenFormat.Float:
                    a.Type<NonNullType<FloatType>>();
                    break;

                case TokenFormat.Date:
                    a.Type<NonNullType<DateType>>();
                    break;

                case TokenFormat.DateTime:
                    a.Type<NonNullType<DateTimeType>>();
                    break;

                // ReSharper disable once RedundantCaseLabel
                case TokenFormat.String:
                default:
                    a.Type<NonNullType<StringType>>();
                    break;
            }
        }
        
        /// <summary>
        /// Create a mapping function for the token string that takes a context and returns a string
        /// </summary>
        public static Func<IResolverContext, string> ConstructFunc(this TokenString ts)
        {
            // Map the token segments to their own function
            var tx = ts.Segments.Select(x => x.ConstructFunc()).ToList();

            // Return a function that joins them all together
            return (c) => string.Join(string.Empty, tx.Select(x => x(c)));
        }

        // ReSharper disable once InconsistentNaming
        public static string GraphQLSafe(this string val)
        {
            return SafeName.Replace(val, "_");
        }

        private static Func<IResolverContext, string> ConstructCultureFunc<TArg>(TokenStringSegment ts) 
            where TArg : struct, IFormattable
        {
            return (c) =>
            {
                var culture = c.GetCulture() ?? CultureInfo.CurrentCulture;
                return ts.Text + (c.Argument<TArg?>(ts.Variable)?.ToString(ts.Format, culture) ?? string.Empty);
            };
        }

        /// <summary>
        /// Create a mapping function that will extract the relevant argument from the context
        /// for the token segment that and return a string value
        /// </summary>
        private static Func<IResolverContext, string> ConstructFunc(this TokenStringSegment ts)
        {
            if (string.IsNullOrWhiteSpace(ts.Variable))
            {
                return (c) => ts.Text;
            }

            switch (ts.FormatType)
            {
                case TokenFormat.Int:
                    return ConstructCultureFunc<int>(ts);

                case TokenFormat.Float:
                    return ConstructCultureFunc<float>(ts);

                case TokenFormat.Date:
                case TokenFormat.DateTime:
                    return ConstructCultureFunc<DateTime>(ts);

                // ReSharper disable once RedundantCaseLabel
                case TokenFormat.String:
                default:
                    return (c) => ts.Text + (c.Argument<string>(ts.Variable) ?? string.Empty);
            }
        }
    }
}
