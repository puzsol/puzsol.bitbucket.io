﻿# ResxQL

ResxQL was created as an example of how our existing resource files could be served as a GraphQL endpoint for comsumption by our web applications and other services.  Resource files (resx) have been around for a long time, and are still a reasonable method of managing multi-lingual strings for consumption in our user interfaces.  What was missing was an easy way to access them through the new micro service, and micro front end landscape we are heading towards.  ResxQL bridges that gap.

## How does it work?

When launched ResxQL will read all the *.resx files in the Content folder and sub-folders into memory.  These are then presented as a GraphQL schema, which can be queried in your desired language using GraphQL.  If the override string does not exist in the desired language it will use the Resx fallback rules to provide a value.  Only keys in the base resx files are listed in the schema.

Each Resx file creates a node of the same name as the file.  If a different language file is available, then that will merge into the base resx language schema.  Different languages can be selected by specifying the language as an optional argument on the root element.

### Multiple language query
For example, providing the query of:
```css
query {
  Message {
    Welcome
  }
  Message_Fr : Message(language: "fr") {
    Welcome
  }
}
```

Will respond with:
```json
{
  "data": {
    "Message": {
      "Welcome": "Welcome to your online piggy bank.  It's not that our tails are curly, but if you try to take anything out we will squeal."
    },
    "Message_Fr": {
      "Welcome": "Bienvenue sur votre tirelire en ligne. Ce n'est pas que nos queues soient bouclées, mais si vous essayez de retirer quoi que ce soit, nous grincerons."
    }
  }
}
```

## Querying the strings
If the string in the resx file contains a C# string format insertion instruction, it will create a string argument for it.  If the insertion is interpolation style (eg {balance}), then it will use that name for the paramter in the schema. If the field has a format definition (eg {balance:c}), then the schema list the parameter with the relevant GraphQL type, instead of as a string.  For date and datetime fields, you must use a custom date string, or it will think it's just a number.

### Named and typed query parameters
For example, in the TermDeposit.resx we have:
```xml
  <data name="Balance" xml:space="preserve">
    <value>Your opening balance was {opening:C2}, if you can wait until {maturation_date:dd MMM yyyy}, we will gift you an extra {increase:C2}. </value>
  </data>
```

Which is documented as:
```js
type Credit_TermDeposit_Messages {
  Balance(increase: Float!, maturation_date: Date!, opening: Float!): String
}
```

The balance string takes three parameters, an opening balance, a maturation_date, and an increase amount.  So if our query looks like:

```css 
query {
  Credit {
    TermDeposit {
      Balance(opening: 50000, increase:4.95, maturation_date: "4-Jul-2051")
    }
  }
}
```

It would return:
```json
{
  "data": {
    "Credit": {
      "TermDeposit": {
        "Balance": "Your opening balance was $50,000.00, if you can wait until 04 Jul 2051, we will gift you an extra $4.95. "
      }
    }
  }
}
```

Or in French:
```css 
query {
  Credit(language: "fr") {
    TermDeposit {
      Balance(opening: 50000, increase:4.95, maturation_date: "4-Jul-2051")
    }
  }
}
```

It would return:
```json
{
  "data": {
    "Credit": {
      "TermDeposit": {
        "Balance": "Votre solde d'ouverture était de 50000, si vous pouvez attendre jusqu'au  04 juil. 2051, nous vous offrirons une 4.95 supplémentaire."
      }
    }
  }
}
```

### Numbered parameters
If the parameter is not named or typed, then it is passed by index as a string.  For example, the message resource of BankHours:
```xml
  <data name="BankHours" xml:space="preserve">
    <value>While there are {0} hours in a day, we only work {1} hours per day... but your debt still increases {0} hours a day.</value>
  </data>
```

When queried has two parameters _0 and _1:
```css
query {
   Message {
    BankHours(_0:"24", _1: "6")
  }
}
```

Returns
```json
{
  "data": {
    "Message": {
      "BankHours": "While there are 24 hours in a day, we only work 6 hours per day... but your debt still increases 24 hours a day."
    }
  }
}
```

**_note_**: As there is no French translation for this string, calling Message(language:"fr") will still return the English version.

## Gratitude

This project makes use of many years of hard work by very smart people. Including but not limited to:
- Microsoft's language and runtime [.NET core](https://docs.microsoft.com/en-us/dotnet/core/) version 3.1 
- [Hotchocolate](https://hotchocolate.io/) - The GraphQL framework for .NET core
- [GraphQL Playground](https://github.com/prisma-labs/graphql-playground) - For making the built-in.presentation of the schema and query testing look and work so cool.

## Used by

_Nobody_

## Disclaimers and warnings
- At the time of writing, this is not in use in production **anywhere**.  No formal support is available, and response to any bug or feature requests might be slow.  Use is at your own risk.  If you do find this useful, drop me a note and I will happily update the list of in-use.
- As stated earlier, all the Resx files are loaded into memory, so if you have large resx files, this may not be the solution for you.
- I used Google translate to convert the examples from English to French, I don't speak French, so if it's bad, that's why. 
- All example text was written in the context of a fictional bank.  If it sounds like yours, it's unintentional, and a gosh darn shame.

## Licensing
This project is covered by the MIT license - enjoy.