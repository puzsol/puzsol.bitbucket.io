﻿namespace ResxQL
{
    using System;

    using GraphTypes;

    using HotChocolate;
    using HotChocolate.AspNetCore;

    using Microsoft.AspNetCore.Builder;
    using Microsoft.AspNetCore.Hosting;
    using Microsoft.Extensions.DependencyInjection;
    using Microsoft.Extensions.Hosting;

    using Serilog;

    public class Startup
    {
        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            try
            {
                services.AddGraphQL(s =>
                    SchemaBuilder.New()
                        .AddServices(s)
                        .AddQueryType<ResourceQueryType>()
                        .Create());
            }
            catch (Exception ex)
            {
                Log.Fatal(ex, "Error in ConfigureServices");
                throw;
            }
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            try
            {
                if (env.IsDevelopment())
                {
                    app.UseDeveloperExceptionPage();
                }

                app.UseGraphQL();
                app.UsePlayground();
            }
            catch (Exception ex)
            {
                Log.Fatal(ex, "Error while serving resources");
            }
        }
    }
}
