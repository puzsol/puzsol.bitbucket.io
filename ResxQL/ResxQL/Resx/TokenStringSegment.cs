﻿namespace ResxQL.Resx
{
    using System.Text.RegularExpressions;

    /// <summary>
    /// One segment of a resource string that consists of some text followed by a variable that could be formatted - we recognize the format and store it for future use
    /// </summary>
    public class TokenStringSegment
    {
        private static readonly Regex IntNumeric = new Regex(@"^[DX]\d{0,2}$", RegexOptions.IgnoreCase);
        private static readonly Regex FloatNumeric = new Regex(@"^[CEFGNP]\d{0,2}$", RegexOptions.IgnoreCase);
        private static readonly Regex DateTime = new Regex(@"^(?:d{1,4}|M{1,4}|y{1,5}|gg?|[hH]{1,2}|mm?|ss?|[fF]{1,6}|tt?|z{1,3}|[.,/:\s-%]|K)+$");
        private static readonly Regex DateOnly = new Regex(@"^(?:d{1,4}|M{1,4}|y{1,5}|gg?|[.,/\s-%])+$");

        public TokenStringSegment(Match m)
        {
            Text = m.Groups["txt"]?.Value ?? string.Empty;

            Variable = m.Groups["var"]?.Value;
            if (string.IsNullOrWhiteSpace(Variable))
            {
                Variable = null;
            } 
            else if (int.TryParse(Variable, out var idx))
            {
                Variable = "_" + idx.ToString();
            }

            Format = m.Groups["fmt"]?.Value.Trim();
            FormatType = TokenFormat.String;
            if (!string.IsNullOrWhiteSpace(Format))
            {
                if (IntNumeric.IsMatch(Format))
                {
                    FormatType = TokenFormat.Int;
                }
                else if (FloatNumeric.IsMatch(Format))
                {
                    FormatType = TokenFormat.Float;
                }
                else if (DateOnly.IsMatch(Format))
                {
                    FormatType = TokenFormat.Date;
                }
                else if (DateTime.IsMatch(Format))
                {
                    FormatType = TokenFormat.DateTime;
                }
            }
        }

        public string Text { get; }

        public string Variable { get; }

        public string Format { get; }

        public TokenFormat FormatType { get; }
    }
}
