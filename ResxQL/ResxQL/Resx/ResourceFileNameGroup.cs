﻿namespace ResxQL.Resx
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml;

    using Serilog;


    /// <summary>
    /// Collect a group of resource files with different language codes and process them into key names with language versions
    /// </summary>
    public class ResourceFileNameGroup
    {
        private readonly Dictionary<string, ResKey> _keys = new Dictionary<string, ResKey>();

        public ResourceFileNameGroup(string name, IEnumerable<ResourceFileInfo> files)
        {
            Log.Information("Reading Resource Group: {name}", name);
            Name = name;
            bool isBase = true;
            try
            {
                // For Core3 we should be able to use the ResXDocument parser - until then, do it through xml methods.
                foreach (var file in files.OrderBy(x => x.Info.Equals(System.Globalization.CultureInfo.CurrentCulture) ? string.Empty : x.Info.Name))
                {
                    Log.Information("Reading file: {file}", file.Path);
                    var doc = new XmlDocument();
                    doc.Load(file.Path);

                    XmlNode root = doc.DocumentElement;
                    var  data = root?.SelectNodes("data");
                    if (data?.Count > 0)
                    {
                        foreach (XmlNode n in data)
                        {
                            if (n?.Attributes == null)
                            {
                                Log.Warning("Missing attributes for {@node}", n);
                                continue;
                            }

                            var key = n.Attributes["name"]?.Value;
                            if (key == null)
                            {
                                Log.Warning("Missing name for {@node}", n);
                                continue;
                            }

                            if (!_keys.TryGetValue(key, out var rk) && isBase)
                            {
                                rk = new ResKey(key);
                                _keys.Add(key, rk);
                            }

                            if (rk == null)
                            {
                                Log.Warning("Missing base language resource for : {@key}", key);
                                continue;
                            }

                            rk.Set(file.Info.Name, n.SelectSingleNode("value")?.InnerText, n.SelectSingleNode("comment")?.InnerText);

                            if (!isBase)
                            {
                                var b = rk.For(System.Globalization.CultureInfo.CurrentCulture.Name);
                                var t = rk.For(file.Info.Name);

                                var keys = new SortedSet<string>(b?.Segments
                                    ?.Where(s => s.Variable != null)
                                    .Select(s => s.Variable) ?? Array.Empty<string>());

                                keys.UnionWith(t?.Segments
                                        ?.Where(s => s.Variable != null)
                                        .Select(s => s.Variable) ?? Array.Empty<string>());

                                foreach(var k in keys)
                                {
                                    var bv = b?.Segments?.FirstOrDefault(s => s.Variable == k);
                                    var tv = t?.Segments?.FirstOrDefault(s => s.Variable == k);

                                    if (bv == null)
                                    {
                                        Log.Error("Resource string has extra variable: {@var}", new { file = file.Name, key, variable = tv?.Variable, type = tv?.FormatType });
                                    }
                                    else if (tv == null)
                                    {
                                        Log.Error("Resource string has missing variable: {@var}", new { file = file.Name, key, variable = bv.Variable, type = bv.FormatType });
                                    }
                                    else if (bv.FormatType != tv.FormatType)
                                    {
                                        Log.Error("Resource string has mismatched variable type: {@var}", new { file = file.Name, key, variable = bv.Variable, types = new[] { bv.FormatType, tv.FormatType } });
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        Log.Warning("Unable to find any data in resx file.");
                    }

                    isBase = false;
                }
            }
            catch (Exception ex)
            {
                Log.Fatal(ex, "Error reading file");
                throw;
            }
        }

        public string Name { get; }

        public IEnumerable<ResKey> Keys => _keys.Values;
    }
}
