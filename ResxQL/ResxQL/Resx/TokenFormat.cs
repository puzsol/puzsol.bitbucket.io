﻿namespace ResxQL.Resx
{
    /// <summary>
    /// The GraphQL type recognized from the string format pattern
    /// </summary>
    public enum TokenFormat
    {
        String,
        Int,
        Float,
        Date,
        DateTime
    }
}
