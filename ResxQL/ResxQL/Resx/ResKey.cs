﻿namespace ResxQL.Resx
{
    using System;
    using System.Collections.Generic;

    using Serilog;

    /// <summary>
    /// Resource Key represents one key in the resource file and collects all the different language versions of it.
    /// It provides a method to search through a language hierarchy to provide a value for the key, falling back to english if none are found.
    /// </summary>
    public class ResKey
    {
        private readonly Dictionary<string, TokenString> _langVersions = new Dictionary<string, TokenString>();

        public ResKey(string name)
        {
            Name = name;
        }

        public string Name { get; }

        public TokenString For(params string[] priority)
        {
            try
            {
                foreach (var lang in priority)
                {
                    if (_langVersions.TryGetValue(lang, out var lv))
                    {
                        return lv;
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Error finding string version for language: {@lang}", priority);
            }

            return null;
        }

        internal void Set(string lang, string val, string desc)
        {
            _langVersions[lang] = new TokenString(val, desc);
        }
    }
}
