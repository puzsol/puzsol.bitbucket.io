﻿namespace ResxQL.Resx
{
    using System.Globalization;
    using System.Linq;

    /// <summary>
    /// Given the full file path to a .RESX file, extract the language code for the file, or default to english.
    /// </summary>
    public class ResourceFileInfo
    {
        public ResourceFileInfo(string path)
        {
            Path = path;
            var filename = System.IO.Path.GetFileNameWithoutExtension(path);
            var parts = filename.Split('.');

            if (parts.Length >= 2)
            {
                try
                {
                    Info = CultureInfo.GetCultureInfo(parts[^1]);
                    Name = string.Join("_", parts.Take(parts.Length - 1));
                }
                catch
                {
                    // nada
                }
            }

            if (Info == null || Name == null)
            {
                Info = CultureInfo.CurrentCulture;
                Name = string.Join("_", parts);
            }
        }

        public string Name { get; }

        public string Path { get; }

        public CultureInfo Info { get; }
    }
}
