﻿namespace ResxQL.Resx
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Text.RegularExpressions;

    /// <summary>
    /// Class that represents a string broken up into text and {formatter:variable} segments.
    /// </summary>
    public class TokenString
    {
        private static readonly Regex FieldRegex = new Regex(@"(?<txt>.*?)(?:\{(?<var>[\d\w]+)(?::(?<fmt>[^}]+))?\})|(?<txt>.+?)$");

        public TokenString(string text, string description)
        {
            Text = text;
            Description = !string.IsNullOrEmpty(description) ? description : null;
            Segments = FieldRegex.Matches(text).Select(x => new TokenStringSegment(x)).ToList();
            HasTokens = Segments?.Count > 0 && Segments.Any(x => x.Variable != null);
        }

        public bool HasTokens { get; }

        public IList<TokenStringSegment> Segments { get; }

        public string Text { get; }

        public string Description { get; }
    }
}
